package com.cn.liukaku.web.admin.service.fallback;

import com.cn.liukaku.common.constants.HttpStatusConstants;
import com.cn.liukaku.common.dto.BaseResult;
import com.cn.liukaku.common.utils.MapperUtils;
import com.cn.liukaku.web.admin.service.AdminService;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;

@Component
public class AdminServiceFallback implements AdminService {

    @Override
    public String login(String loginCode, String password) {
        BaseResult baseResult = BaseResult.notOk(Lists.newArrayList(
                new BaseResult.Error(String.valueOf(HttpStatusConstants.BAD_GATEWAY.getStatus()),
                        HttpStatusConstants.BAD_GATEWAY.getContent())));

        try {
            return MapperUtils.obj2json(baseResult);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "服务器解析相应错误";
    }

}
