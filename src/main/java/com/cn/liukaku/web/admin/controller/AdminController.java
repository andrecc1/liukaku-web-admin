package com.cn.liukaku.web.admin.controller;


import com.cn.liukaku.web.admin.service.AdminService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

@Controller
public class AdminController {

    @Resource
    private AdminService adminService;

    @RequestMapping(value = {"", "login"}, method = RequestMethod.GET)
    public String login() {
        String lid = "3";
        String password = "123123";
        String json = adminService.login(lid, password);

        System.out.println(json);

        return "index";
    }
}
